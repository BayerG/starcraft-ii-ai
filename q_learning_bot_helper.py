import os
import yaml
import numpy
import tensorflow
from constants import *


class QLearningBotHelper:
    def __init__(self, train=False, learning_rate=LEARNING_RATE, current_step=0, state_queue=None, qs_queue=None,
                 batch_queue=None, manager_queue=None):
        self.train = train
        self.learning_rate = learning_rate
        self.current_step = current_step
        self.state_queue = state_queue
        self.qs_queue = qs_queue
        self.batch_queue = batch_queue
        self.manager_queue = manager_queue

        # Initialize TensorFlow session.
        config = tensorflow.compat.v1.ConfigProto()
        config.gpu_options.per_process_gpu_memory_fraction = GPU_MEMORY_FRACTION
        config.gpu_options.allow_growth = True
        session = tensorflow.compat.v1.InteractiveSession(config=config)
        tensorflow.compat.v1.keras.backend.set_session(session)

        # Initialize model.
        convolutional_model = tensorflow.keras.models.Sequential(layers=CONVOLUTIONAL_LAYERS)
        convolutional_input = tensorflow.keras.Input(
            shape=(REPRESENTATION_WIDTH, REPRESENTATION_HEIGHT, CHANNELS_COUNT))
        convolutional_output = convolutional_model(convolutional_input)
        features_input = tensorflow.keras.Input(shape=(FEATURES_COUNT,))
        dense_model = tensorflow.keras.models.Sequential(layers=DENSE_LAYERS)
        model_output = dense_model(tensorflow.concat([convolutional_output, features_input], axis=1))
        if os.path.exists(os.path.join(EXPERIMENT_DIRECTORY, MODEL_FILENAME)):
            self.model = tensorflow.keras.models.load_model(os.path.join(EXPERIMENT_DIRECTORY, MODEL_FILENAME))
        else:
            self.model = tensorflow.keras.Model([convolutional_input, features_input], model_output)
            self.model.compile(optimizer=OPTIMIZER(learning_rate=self.learning_rate), loss=LOSS, metrics=METRICS)

        # Initialize TensorBoard logger.
        file_writer = tensorflow.summary.create_file_writer(logdir=os.path.join(EXPERIMENT_DIRECTORY, LOGS_FILENAME))
        file_writer.set_as_default()
        self.logger_callback = CustomCallback(self.model, self.current_step)

    def run(self):
        while True:
            # Handle state and qs queues.
            if not self.state_queue.empty():
                view, features = self.state_queue.get()
                action_values = self.model.predict(
                    [
                        view.reshape(-1, REPRESENTATION_WIDTH, REPRESENTATION_HEIGHT, CHANNELS_COUNT),
                        features.reshape(-1, FEATURES_COUNT)
                    ])[0]
                self.qs_queue.put(action_values)

            if self.train:
                # Handle batch queue.
                if not self.batch_queue.empty():
                    # Perform Q-learning step.
                    states, actions, rewards, new_states, is_terminals = self.batch_queue.get()

                    new_views, new_features = numpy.stack(new_states[:, 0]), numpy.stack(new_states[:, 1])
                    new_states_qs = self.predict(new_views, new_features)
                    ys = rewards + DISCOUNT * numpy.max(new_states_qs, axis=1) * is_terminals
                    views, features = numpy.stack(states[:, 0]), numpy.stack(states[:, 1])
                    states_qs = self.predict(views, features)
                    numpy.put_along_axis(states_qs, actions.reshape(-1, 1), ys.reshape(-1, 1), axis=1)

                    # Train the model.
                    self.model.fit(
                        [
                            views.reshape(-1, REPRESENTATION_WIDTH, REPRESENTATION_HEIGHT, CHANNELS_COUNT),
                            features.reshape(-1, FEATURES_COUNT)
                        ],
                        states_qs, batch_size=len(views), callbacks=[self.logger_callback], verbose=0)

            # Handle manager queue (log data or save the model).
            while not self.manager_queue.empty():
                message = self.manager_queue.get()
                if message[0] == 'log':
                    _, name, data, step = message
                    tensorflow.summary.scalar(name, data, step)
                elif message[0] == 'save':
                    with open(os.path.join(EXPERIMENT_DIRECTORY, BOT_PARAMETERS_FILENAME), 'r') as file:
                        parameters = yaml.safe_load(file)
                    with open(os.path.join(EXPERIMENT_DIRECTORY, BOT_PARAMETERS_FILENAME), 'w') as file:
                        yaml.dump(parameters, file)
                    self.model.save(os.path.join(EXPERIMENT_DIRECTORY, MODEL_FILENAME), save_format='h5')
                    tensorflow.summary.flush()

    def predict(self, input1, input2):
        prediction = self.model.predict(
            [
                input1.reshape(-1, REPRESENTATION_WIDTH, REPRESENTATION_HEIGHT, CHANNELS_COUNT),
                input2.reshape(-1, FEATURES_COUNT)
            ],
            batch_size=len(input1))
        return prediction


class CustomCallback(tensorflow.keras.callbacks.Callback):
    """
    A helper callback class for simple TensorBoard loss and accuracy logging.
    """
    def __init__(self, model, step):
        super().__init__()
        self.model = model
        self.step = step

    def on_epoch_end(self, epoch, logs=None):
        tensorflow.summary.scalar('loss', logs['loss'], step=self.step)
        tensorflow.summary.scalar('accuracy', logs['accuracy'], step=self.step)
        self.step += 1
