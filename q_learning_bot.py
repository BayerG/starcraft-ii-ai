import numpy
import random
import yaml
import os
import multiprocessing
import cv2
from sc2.data import Result
from base_bot import BaseBot
from replay_memory import ReplayMemory
from constants import *
from q_learning_bot_helper import QLearningBotHelper


class QLearningBot(BaseBot):
    """
    Bot that chooses available actions depending on maximum Q-value predicted by the machine learning model: holding
    position, defending or attacking. Includes training workers and stalkers, expanding, building pylons gateway,
    cybernetics core and robotics facility. Also sends observers for scouting enemy bases.
    """
    render: bool
    verbose: int
    train: bool
    epsilon: float
    epsilon_decay: float
    discount: float
    replay_size: int
    batch_size: int
    learning_rate: float
    current_step: int
    current_episode: int

    def __init__(self, render=False, verbose=0, train=False, epsilon=EPSILON, epsilon_decay=EPSILON_DECAY,
                 discount=DISCOUNT, replay_size=REPLAY_SIZE, batch_size=BATCH_SIZE, learning_rate=LEARNING_RATE,
                 current_step=0, current_episode=0):
        super().__init__()

        # Initialize bot parameters.
        self.render = render
        self.verbose = verbose
        self.train = train
        self.epsilon = epsilon
        self.epsilon_decay = epsilon_decay
        self.discount = discount
        self.replay_size = replay_size
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.current_step = current_step
        self.current_episode = current_episode

        self.units_tags_info = None
        self.destroyed_units_tags = None
        self.current_state = None
        self.current_reward = None
        self.episode_return = None
        self.current_discount = None

        # Initialize multiprocessing queues and helper process.
        self.state_queue = multiprocessing.Queue()  # States for neural network to predict in training process.
        self.qs_queue = multiprocessing.Queue()  # Predicted Q values of neural network from training process.
        self.batch_queue = multiprocessing.Queue()  # Samples from memory to batch in training process.
        self.manager_queue = multiprocessing.Queue()  # Logs(rewards, epsilons and returns) and saving signals.
        self.helper_process = multiprocessing.Process(
            target=self.run_helper_process,
            args=(
                self.train,
                self.learning_rate,
                self.current_step,
                self.state_queue,
                self.qs_queue,
                self.batch_queue,
                self.manager_queue
            )
        )
        self.helper_process.start()

        # Initialize replay memory.
        if os.path.exists(os.path.join(EXPERIMENT_DIRECTORY, MEMORY_FILENAME)):
            self.memory = ReplayMemory.load(os.path.join(EXPERIMENT_DIRECTORY, MEMORY_FILENAME))
        else:
            self.memory = ReplayMemory(self.replay_size)

    @staticmethod
    def load(filename):
        """
        Loads bot from file.
        """
        with open(filename) as file:
            parameters = yaml.safe_load(file)
        bot = QLearningBot(**parameters)
        return bot

    def save(self, filename):
        """
        Saves bot to file.
        """
        # Save bot parameters.
        parameters = {}
        for attribute in [
            'render',
            'verbose',
            'train',
            'epsilon',
            'epsilon_decay',
            'discount',
            'replay_size',
            'batch_size',
            'learning_rate',
            'current_step',
            'current_episode'
        ]:
            parameters[attribute] = self.__getattribute__(attribute)
        with open(filename, 'w') as file:
            yaml.dump(parameters, file)

        # Signal helper process to save the model.
        self.manager_queue.put(('save',))

        # Save replay memory.
        self.memory.save(os.path.join(EXPERIMENT_DIRECTORY, MEMORY_FILENAME))

    async def on_start(self):
        await super().on_start()
        if self.train:
            self.units_tags_info = {}
            self.destroyed_units_tags = []
            self.current_state = None
            self.current_action = None
            self.current_reward = None
            self.episode_return = 0
            self.current_discount = 1

    async def on_unit_destroyed(self, unit_tag):
        if self.train:
            self.destroyed_units_tags.append(unit_tag)

    async def on_end(self, game_result):
        if not self.train:
            return

        # Get reward depending on game result and update episode return.
        self.current_reward += VICTORY_REWARD if game_result == Result.Victory else DEFEAT_REWARD
        if self.verbose >= 1:
            print(f'Total reward income {self.current_reward}')

        # Update episode return.
        if self.current_reward is not None:
            self.episode_return += self.current_discount * self.current_reward

        # Mark last replay transition as terminal and add the appropriate reward (victory or defeat).
        if self.memory.size > 0:
            last_index = (self.memory.store_index - 1) % self.memory.max_size
            self.memory.is_terminals[last_index] = True
            self.memory.rewards[last_index] = self.current_reward

        self.manager_queue.put(('log', 'reward', self.current_reward, self.current_step))
        self.manager_queue.put(('log', 'return', self.episode_return, self.current_episode))

        self.current_step += 1
        self.current_episode += 1

        if self.verbose >= 1:
            print(f'Episode finished: return {self.episode_return}')

        self.save(os.path.join(EXPERIMENT_DIRECTORY, BOT_PARAMETERS_FILENAME))

    async def act(self):
        """
        Performs one of the available actions depending on values of actions predicted by the model. If train, also
        trains the model while playing.
        """
        # Observe current state.
        new_state = self.representation()

        if self.train:
            # Update unit tags dictionary for unit destroying rewards generation.
            for unit in self.units + self.structures + self.enemy_units + self.enemy_structures:
                self.units_tags_info[unit.tag] = (unit.type_id, unit.is_enemy)

            # Update episode return.
            if self.current_reward is not None:
                self.episode_return += self.current_discount * self.current_reward

            # Calculate reward.
            self.current_reward = self.get_destroying_reward()
            if self.verbose >= 1:
                print(f'Total reward income {self.current_reward}')

            # Store transition in replay memory and update current state.
            if self.current_state is not None \
                    and self.current_action is not None \
                    and self.current_reward is not None:
                self.memory.store((self.current_state, self.current_action, self.current_reward, False))

            # Send logging information.
            if self.current_reward is not None:
                self.manager_queue.put(('log', 'reward', self.current_reward, self.current_step))
            self.manager_queue.put(('log', 'epsilon', self.epsilon, self.current_step))

            self.current_state = new_state

            # Perform Q-learning step.
            self.experience_memory()

        # Choose an action.
        if self.train:
            # Epsilon greedy.
            if random.random() > self.epsilon:
                self.state_queue.put(new_state)
                action_values = self.qs_queue.get()
                self.current_action = numpy.argmax(action_values)
                if self.verbose >= 2:
                    print(f'Action {ACTIONS[self.current_action]} (greedy {action_values})')
            else:
                self.current_action = random.randrange(len(ACTIONS))
                if self.verbose >= 2:
                    print(f'Action {ACTIONS[self.current_action]} (non-greedy)')
        else:
            # Greedy.
            self.state_queue.put(new_state)
            action_values = self.qs_queue.get()
            self.current_action = numpy.argmax(action_values)
            if self.verbose >= 2:
                print(f'Action {ACTIONS[self.current_action]} (greedy {action_values})')

        if self.train:
            self.current_discount *= self.discount
            self.epsilon *= self.epsilon_decay

            self.current_step += 1

    @staticmethod
    def run_helper_process(train, learning_rate, current_step, state_queue, qs_queue, batch_queue, manager_queue):
        QLearningBotHelper(train, learning_rate, current_step, state_queue, qs_queue, batch_queue, manager_queue).run()

    def representation(self):
        view = numpy.zeros((REPRESENTATION_WIDTH, REPRESENTATION_HEIGHT, CHANNELS_COUNT))

        for unit in self.units + self.structures + self.enemy_units + self.enemy_structures:
            x = int(REPRESENTATION_WIDTH * unit.position.x / self.game_info.map_size.width)
            y = int(REPRESENTATION_HEIGHT * unit.position.y / self.game_info.map_size.height)
            value = self.calculate_unit_value(unit.type_id)
            intensity = (value.minerals + value.vespene) * COST_INTENSITY_RATIO
            view[x, y, 0 if not unit.is_enemy else 1] = max(view[x, y, 0 if not unit.is_enemy else 1], intensity)

        if self.render:
            allies = view[:, :, 0]
            enemies = view[:, :, 1]
            rgb_image = numpy.dstack((allies, numpy.zeros((REPRESENTATION_WIDTH, REPRESENTATION_HEIGHT)), enemies))
            rgb_image = rgb_image * 255 * RENDER_INTENSITY
            rgb_image = numpy.minimum(rgb_image, 255).astype(numpy.uint8)
            flipped = cv2.transpose(cv2.flip(rgb_image, 1))
            resized = cv2.resize(flipped, dsize=None, fx=RENDER_SCALE, fy=RENDER_SCALE, interpolation=cv2.INTER_NEAREST)
            cv2.imshow('Representation', resized)
            cv2.waitKey(1)

        minerals_ratio = self.minerals / (LOCATIONS_COUNT * TOTAL_MINERALS_PER_LOCATION)
        vespene_ratio = self.vespene / (LOCATIONS_COUNT * TOTAL_VESPENE_PER_LOCATION)
        supply_ratio = self.supply_used / MAX_SUPPLY
        army_supply_ratio = self.supply_army / MAX_SUPPLY
        features = numpy.array([minerals_ratio, vespene_ratio, supply_ratio, army_supply_ratio])

        return [view, features]

    def experience_memory(self):
        # Sample random minibatch of transitions from replay memory and perform Q-learning step.
        if self.memory.size > 1 and self.batch_queue.empty():
            states, actions, rewards, new_states, is_terminals = self.memory.sample(self.batch_size)
            self.batch_queue.put((states, actions, rewards, new_states, is_terminals))

    def get_destroying_reward(self):
        reward = 0

        for destroyed_unit_tag in self.destroyed_units_tags:
            try:
                destroyed_unit_type, is_enemy = self.units_tags_info[destroyed_unit_tag]
                unit_cost = self.calculate_unit_value(destroyed_unit_type)
                reward_income = (unit_cost.minerals + unit_cost.vespene) \
                                / (LOCATIONS_COUNT * (TOTAL_MINERALS_PER_LOCATION + TOTAL_VESPENE_PER_LOCATION)) \
                                * (VICTORY_REWARD if is_enemy else DEFEAT_REWARD)
                reward += reward_income
                if self.verbose >= 3:
                    print(f'Unit destroyed: {destroyed_unit_type} is enemy {is_enemy}')
            except KeyError:
                pass
        self.destroyed_units_tags = []

        return reward
