import random
from constants import *
from base_bot import BaseBot


class RandomActionsBot(BaseBot):
    async def act(self):
        action = random.choice(ACTIONS)
        if action == 'defend':
            await self.defend()
        elif action == 'attack':
            await self.attack()