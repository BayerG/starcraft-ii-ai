import numpy
from constants import *


class ReplayMemory:
    """
    A helper class for quick access to replay memory.
    """
    def __init__(self, max_size):
        self.max_size = max_size

        self.store_index = 0
        self.size = 0

        self.views = numpy.empty((self.max_size, REPRESENTATION_WIDTH, REPRESENTATION_HEIGHT, CHANNELS_COUNT),
                                 dtype=float)
        self.features = numpy.empty((self.max_size, FEATURES_COUNT), dtype=float)
        self.actions = numpy.empty(self.max_size, dtype=int)
        self.rewards = numpy.empty(self.max_size, dtype=float)
        self.is_terminals = numpy.empty(self.max_size, dtype=bool)

    @staticmethod
    def load(filename):
        data = numpy.load(filename)

        assert(int(data['store_index'])
               <= int(data['size'])
               == len(data['views'])
               == len(data['features'])
               == len(data['actions'])
               == len(data['rewards'])
               == len(data['is_terminals'])
               <= int(data['max_size']))

        max_size = int(data['max_size'])
        memory = ReplayMemory(max_size)
        memory.store_index = int(data['store_index'])
        memory.size = int(data['size'])

        memory.views[:memory.size] = data['views']
        memory.features[:memory.size] = data['features']
        memory.actions[:memory.size] = data['actions']
        memory.rewards[:memory.size] = data['rewards']
        memory.is_terminals[:memory.size] = data['is_terminals']

        return memory

    def save(self, filename):
        with open(filename, 'wb') as file:
            numpy.savez(file,
                        max_size=self.max_size,
                        size=self.size,
                        store_index=self.store_index,
                        views=self.views[:self.size],
                        features=self.features[:self.size],
                        actions=self.actions[:self.size],
                        rewards=self.rewards[:self.size],
                        is_terminals=self.is_terminals[:self.size])

    def store(self, transition):
        state, action, reward, is_terminal = transition
        view, features = state
        self.views[self.store_index] = view
        self.features[self.store_index] = features
        self.actions[self.store_index] = action
        self.rewards[self.store_index] = reward
        self.is_terminals[self.store_index] = is_terminal

        self.store_index = (self.store_index + 1) % self.max_size
        self.size = min(self.size + 1, self.max_size)

    def sample(self, amount):
        if self.size > 1:
            indices_for_choice = list(range(self.size))
            del indices_for_choice[(self.store_index - 1) % self.max_size]
            indices = numpy.random.choice(indices_for_choice, amount)
        else:
            indices = numpy.array([], dtype=int)
        states = numpy.array(list(zip(self.views[indices, :], self.features[indices, :])))
        actions = self.actions[indices]
        rewards = self.rewards[indices]
        next_indices = (indices + 1) % self.max_size
        new_states = numpy.array(list(zip(self.views[next_indices, :], self.features[next_indices, :])))
        is_terminals = self.is_terminals[indices]
        return states, actions, rewards, new_states, is_terminals
