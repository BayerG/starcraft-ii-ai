import random
import math
from sklearn.cluster import AgglomerativeClustering
from sc2.bot_ai import BotAI
from sc2.units import Units
from constants import *


class BaseBot(BotAI):
    def __init__(self):
        super().__init__()
        self.scouting_location = None
        self.is_scout_retreating = None
        self.attacking_location = None
        self.current_action = None
        self.last_action_time = None
        self.last_decision_time = None
        self.agglomerative_clustering = AgglomerativeClustering(
            n_clusters=None,
            distance_threshold=ARMY_CLUSTERING_THRESHOLD,
            linkage='single'
        )

    async def on_start(self):
        self.scouting_location = self.enemy_start_locations[0]
        self.is_scout_retreating = False
        self.attacking_location = self.enemy_start_locations[0]
        self.current_action = None
        self.last_action_time = -60 / ACTIONS_PER_MINUTE
        self.last_decision_time = -60 / DECISIONS_PER_MINUTE

    async def on_step(self, iteration):
        if self.time >= self.last_action_time + 60 / ACTIONS_PER_MINUTE:
            # Controlling step.
            await self.distribute_workers()
            await self.scout()
            await self.prismatic_alignment()

            if self.current_action is not None:
                if ACTIONS[self.current_action] == 'defend':
                    await self.defend()
                elif ACTIONS[self.current_action] == 'attack':
                    await self.attack()

            # Developing step.
            # If we have resources for expansion or if enemy has more bases than we dd, then expand.
            if (self.can_afford(NEXUS) or self.structures(NEXUS).amount < self.enemy_structures(NEXUS).amount) \
                    and not self.already_pending(NEXUS) \
                    and self.units(PROBE).exists:
                await self.expand_now()
            elif not self.units(PROBE).exists:
                for nexus in self.structures(NEXUS).ready.idle:
                    self.do(nexus.train(PROBE))
            else:
                await self.create_units()
                await self.create_buildings()
                await self.research_upgrades()
            self.last_action_time = self.time

        if self.time >= self.last_decision_time + 60 / DECISIONS_PER_MINUTE:
            await self.act()
            self.last_decision_time = self.time

    async def create_units(self):
        """
        Trains units (workers, zealots, stalkers, sentry and immortals).
        """
        # Train the offensive force - zealots, stalkers, immortals and voidrays. Also train observers, if there's no
        # one.
        for gateway in self.structures(GATEWAY).ready.idle:
            if not self.structures(CYBERNETICSCORE).ready.exists:
                self.do(gateway.train(ZEALOT))
            else:
                if self.structures(GATEWAY).ready.amount > 1 \
                        and gateway == self.structures(GATEWAY).ready.sorted(key=lambda unit: unit.tag).first:
                    self.do(gateway.train(ZEALOT))
                else:
                    self.do(gateway.train(STALKER))

        for stargate in self.structures(STARGATE).idle:
            if not self.structures(FLEETBEACON).ready.exists:
                self.do(stargate.train(VOIDRAY))
            else:
                self.do(stargate.train(CARRIER))

        for robotics in self.structures(ROBOTICSFACILITY).idle:
            if not self.units(OBSERVER).ready.exists:
                self.do(robotics.train(OBSERVER))
            else:
                self.do(robotics.train(IMMORTAL))

        # Train workers.
        workers_per_base \
            = WORKERS_PER_MINERALS + GEYSERS_PER_BASE * (WORKERS_PER_VESPENE - WORKERS_IN_VESPENE)
        if self.units(PROBE).amount < MAX_WORKERS \
                and self.units(PROBE).amount < self.structures(NEXUS).ready.amount * workers_per_base:
            for nexus in self.structures(NEXUS).ready.idle:
                self.do(nexus.train(PROBE))

    async def create_buildings(self):
        """
        Creates buildings (pylons, assimilators and offensive force buildings).
        """
        # Build pylons.
        if self.supply_left < SUPPLY_LEFT_CRITICAL \
                and not self.already_pending(PYLON) \
                and self.supply_cap < MAX_SUPPLY:
            nexuses = self.structures(NEXUS).ready
            if nexuses.exists:
                if self.structures(PYLON).amount > 0:
                    pylon_position = nexuses.first.position.random_on_distance(BASE_PYLON_DISTANCE)
                else:
                    pylon_position = nexuses.first.position + PYLON_ENEMY_BASE_OFFSET \
                                     * (self.enemy_start_locations[0].position - nexuses.first.position).normalized
                await self.build(PYLON, near=pylon_position)

        # Build assimilators.
        for nexus in self.structures(NEXUS).ready:
            vespene_geysers = self.vespene_geyser.closer_than(GEYSERS_BASE_MAX_DISTANCE, nexus)
            for vespene_geyser in vespene_geysers:
                if not self.structures(ASSIMILATOR).closer_than(1, vespene_geyser).exists:
                    await self.build(ASSIMILATOR, near=vespene_geyser)

        # Build offensive force buildings (gateways, forge, cybernetics core, robotics facility and twilight council).
        if self.structures(PYLON).ready.exists:
            pylon = self.structures(PYLON).ready.random

            if self.structures(GATEWAY).amount < int(self.structures(NEXUS).ready.amount * GATEWAY_NEXUSES_RATIO) \
                    and not self.already_pending(GATEWAY):
                await self.build(GATEWAY, near=pylon)

            if self.structures(GATEWAY).ready.exists \
                    and not self.structures(CYBERNETICSCORE) \
                    and not self.already_pending(CYBERNETICSCORE):
                await self.build(CYBERNETICSCORE, near=pylon)

            if self.structures(CYBERNETICSCORE).ready.exists \
                    and not self.structures(ROBOTICSFACILITY) \
                    and not self.already_pending(ROBOTICSFACILITY):
                await self.build(ROBOTICSFACILITY, near=pylon)

            if self.structures(CYBERNETICSCORE).ready.exists \
                    and not self.structures(STARGATE) \
                    and not self.already_pending(STARGATE):
                await self.build(STARGATE, near=pylon)

            if not self.structures(FORGE) \
                    and not self.already_pending(FORGE):
                await self.build(FORGE, near=pylon)

            if self.structures(CYBERNETICSCORE).ready.exists \
                    and not self.structures(TWILIGHTCOUNCIL) \
                    and not self.already_pending(TWILIGHTCOUNCIL) \
                    and (PROTOSSGROUNDWEAPONSLEVEL1 in self.state.upgrades
                         or PROTOSSGROUNDARMORSLEVEL1 in self.state.upgrades
                         or PROTOSSSHIELDSLEVEL1 in self.state.upgrades):
                await self.build(TWILIGHTCOUNCIL, near=pylon)

            if self.structures(STARGATE).ready.exists \
                    and not self.structures(FLEETBEACON) \
                    and not self.already_pending(FLEETBEACON) \
                    and (PROTOSSAIRWEAPONSLEVEL1 in self.state.upgrades
                         or PROTOSSAIRARMORSLEVEL1 in self.state.upgrades):
                await self.build(FLEETBEACON, near=pylon)

    async def research_upgrades(self):
        """
        Researches weapons, armor and shield upgrades. Also research charge for zealots in twilight council.
        """
        for forge in self.structures(FORGE).idle:
            for upgrade in FORGE_UPGRADES:
                self.do(forge.research(upgrade))
                if not forge.is_idle:
                    break
        for cybernetics_core in self.structures(CYBERNETICSCORE).idle:
            for upgrade in CYBERNETICSCORE_UPGRADES:
                self.do(cybernetics_core.research(upgrade))
                if not cybernetics_core.is_idle:
                    break
        for twilight in self.structures(TWILIGHTCOUNCIL).ready.idle:
            for upgrade in TWILIGHT_UPGRADES:
                self.do(twilight.research(upgrade))
                if not twilight.is_idle:
                    break

    async def scout(self):
        """
        Chooses scouting location and moves observer to it. If there's no observer, moves probe.
        """
        # Choose a unit for scouting.
        if not self.units(OBSERVER).exists:
            if self.units(PROBE).exists:
                scout = self.units(PROBE).sorted(key=lambda unit: unit.tag).first
            else:
                scout = None
        else:
            scout = self.units(OBSERVER).sorted(key=lambda unit: unit.tag).first
        if scout is None:
            return
        # If scout can be attacked then retreat.
        for enemy_unit in self.enemy_units + self.enemy_structures:
            if (scout.type_id == PROBE and enemy_unit.can_attack and enemy_unit.type_id != PROBE
                    or scout.type_id == OBSERVER and (enemy_unit.is_detector or enemy_unit.type_id == PHOTONCANNON)) \
                    and scout.distance_to(enemy_unit) <= scout.sight_range:
                self.do(scout.move(self.start_location))
                self.is_scout_retreating = True
                return
        # If scout has reached scouting location or if scout has finished retreating then change scouting location.
        if scout.distance_to(self.scouting_location) <= SCOUTING_LOCATION_CHANGE_DISTANCE \
                or self.is_scout_retreating:
            self.is_scout_retreating = False
            await self.change_scouting_location()
        self.do(scout.move(self.scouting_location))

    async def prismatic_alignment(self):
        for voidray in self.units(VOIDRAY):
            for enemy_unit in self.enemy_units:
                if voidray.distance_to(enemy_unit) <= voidray.sight_range and enemy_unit.is_armored:
                    self.do(voidray(PRISMATICALIGNMENT))

    async def act(self):
        pass

    async def hold(self):
        """
        Groups up army units groups and holds the position.
        """
        army = Units([], self)
        for army_unit_type in ARMY_UNIT_TYPES:
            army.extend(self.units(army_unit_type))
        for units_group in self.cluster_units(army):
            for unit in units_group:
                self.do(unit.move(units_group.center))

    async def defend(self):
        """
        Groups up army units groups and defends base under attack.
        """
        army = Units([], self)
        for army_unit_type in ARMY_UNIT_TYPES:
            army.extend(self.units(army_unit_type))
        if army.empty:
            return
        # Find first nexus which is under attack. If there's no such, just move to the nexus which is the closest to
        # enemy base.
        target_point = self.start_location
        for nexus in self.structures(NEXUS):
            if nexus.distance_to(self.enemy_start_locations[0]) \
                    < target_point.distance_to(self.enemy_start_locations[0]):
                target_point = nexus.position
        for nexus in self.structures(NEXUS):
            for enemy_unit in self.enemy_units + self.enemy_structures:
                if nexus.distance_to(enemy_unit) <= BASE_UNDER_ATTACK_DISTANCE:
                    target_point = nexus.position
        for unit_group in self.cluster_units(army):
            # If group center is close enough to defending point, then move to this point through attack, else move
            # towards it according to offset.
            # if unit_group.center.distance_to(target_point) < ARMY_CENTER_DEFENDING_DISTANCE:
            #     for unit in unit_group:
            #         self.do(unit.attack(target_point))
            # else:
            moving_point = unit_group.center + ARMY_CENTER_DEFENDING_OFFSET * (target_point - unit_group.center).normalized
            if not self.in_pathing_grid(moving_point):
                moving_point = target_point
            for unit in unit_group:
                self.do(unit.attack(moving_point))

    async def attack(self):
        """
        Groups up army units groups and attacks enemy bases.
        """
        army = Units([], self)
        for army_unit_type in ARMY_UNIT_TYPES:
            army.extend(self.units(army_unit_type))
        if army.empty:
            return
        for unit_group in self.cluster_units(army):
            # Attack enemy units and structures. If there's no such, attack enemy location. If there's still no such and
            # group is at the attacking location, change attacking location to other expansion location.
            if not (self.enemy_structures + self.enemy_units).empty:
                target_point = (self.enemy_structures + self.enemy_units).closest_to(unit_group.center).position
            else:
                if unit_group.center.distance_to(self.attacking_location) <= ATTACKING_LOCATION_CHANGE_DISTANCE:
                    self.attacking_location = random.choice(list(self.expansion_locations))
                target_point = self.attacking_location
            attacking_point \
                = unit_group.center + ARMY_CENTER_ATTACKING_OFFSET * (target_point - unit_group.center).normalized
            if not self.in_pathing_grid(attacking_point):
                attacking_point = target_point
            for unit in unit_group:
                self.do(unit.attack(attacking_point))

    async def change_scouting_location(self):
        """
        If current scouting location in enemy start location, then changes scouting location to other enemy expansion
        location, that is not his start location, and vice versa.
        """
        if self.enemy_structures(NEXUS).exists:
            if self.scouting_location == self.enemy_start_locations[0]:
                min_distance = math.inf
                for expansion in self.expansion_locations:
                    is_free_expansion = True
                    for nexus in self.enemy_structures(NEXUS):
                        if nexus.distance_to(expansion) < self.EXPANSION_GAP_THRESHOLD:
                            is_free_expansion = False
                            break
                    distance = await self._client.query_pathing(self.enemy_start_locations[0], expansion)
                    if distance is None:
                        continue
                    if is_free_expansion and distance < min_distance:
                        self.scouting_location = expansion
                        min_distance = distance
            else:
                self.scouting_location = self.enemy_start_locations[0]
        else:
            ordered_expansions = sorted(self.expansion_locations,
                                        key=lambda location: self.enemy_start_locations[0].distance_to(location))
            enemy_expansions = ordered_expansions[:SCOUTING_LOCATIONS_COUNT]
            self.scouting_location = random.choice(enemy_expansions)

    def cluster_units(self, units):
        """
        Clusters units by their positions and returns a list of unit groups.
        """
        unit_groups = []
        units_positions = list(map(lambda unit: unit.position, units))
        if len(units) == 1:
            unit_groups.append(Units([units[0]], self))
        elif len(units) > 1:
            self.agglomerative_clustering.fit(units_positions)
            n_clusters = self.agglomerative_clustering.n_clusters_
            labels = self.agglomerative_clustering.labels_
            for group_id in range(n_clusters):
                unit_group = Units([units[i] for i in range(len(labels)) if labels[i] == group_id], self)
                unit_groups.append(unit_group)
        return unit_groups

