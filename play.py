import os
import datetime
import yaml
import sc2
from sc2.player import Bot, Computer
from sc2.data import Result
from constants import *
from random_actions_bot import RandomActionsBot
from q_learning_bot import QLearningBot

if __name__ == '__main__':
    # Load experiment and initialize bot.
    with open(os.path.join(EXPERIMENT_DIRECTORY, EXPERIMENT_PARAMETERS_FILENAME)) as file:
        parameters = yaml.safe_load(file)

    if parameters['bot'] == 'RandomActionsBot':
        bot = RandomActionsBot()
    elif parameters['bot'] == 'QLearningBot':
        bot = QLearningBot.load(os.path.join(EXPERIMENT_DIRECTORY, BOT_PARAMETERS_FILENAME))
    else:
        raise ValueError('Incorrect bot type')

    while parameters['played'] < parameters['episodes']:
        game_map = sc2.maps.get(MAP)
        player1 = Bot(BOT_RACE, bot, name='Bot')
        player2 = Computer(RACES[parameters['computer_race']], DIFFICULTY_LEVELS[parameters['computer_difficulty']])
        result = sc2.run_game(game_map, [player1, player2], realtime=parameters['realtime'])
        if result == Result.Victory:
            parameters['victories'] += 1
        parameters['played'] += 1
        with open(os.path.join(EXPERIMENT_DIRECTORY, REPORT_FILENAME), 'a') as file:
            file.write(
                f'{datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f")}: {parameters["bot"]} '
                f'{"victory" if result == Result.Victory else "defeat" if result == Result.Defeat else "tie"}\n'
            )
        with open(os.path.join(EXPERIMENT_DIRECTORY, EXPERIMENT_PARAMETERS_FILENAME), 'w') as file:
            yaml.dump(parameters, file)

    with open(os.path.join(EXPERIMENT_DIRECTORY, REPORT_FILENAME), 'a') as file:
        file.write(
            f'{datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f")}: '
            f'{parameters["bot"]} played {parameters["episodes"]} episodes, '
            f'{parameters["victories"]} victories, '
            f'winrate {parameters["victories"] / parameters["episodes"] * 100 :.2f}%\n'
        )

    if isinstance(bot, QLearningBot):
        bot.helper_process.terminate()
