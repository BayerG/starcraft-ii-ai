absl-py==0.9.0
aiohttp==3.6.2
astor==0.8.1
async-timeout==3.0.1
attrs==19.3.0
burnysc2==4.11.11
cachetools==4.0.0
certifi==2019.11.28
chardet==3.0.4
future==0.18.2
gast==0.2.2
google-auth==1.11.3
google-auth-oauthlib==0.4.1
google-pasta==0.2.0
grpcio==1.27.2
h5py==2.10.0
idna==2.9
joblib==0.14.1
Keras-Applications==1.0.8
Keras-Preprocessing==1.1.0
Markdown==3.2.1
mpyq==0.2.5
multidict==4.7.5
numpy==1.18.1
oauthlib==3.1.0
opencv-python==4.2.0.32
opt-einsum==3.2.0
Pillow==7.0.0
portpicker==1.3.1
protobuf==3.11.3
pyasn1==0.4.8
pyasn1-modules==0.2.8
pyglet==1.5.0
PyYAML==5.3.1
requests==2.23.0
requests-oauthlib==1.3.0
rsa==4.0
s2clientprotocol==4.11.4.78285.0
scikit-learn==0.22.2.post1
scipy==1.4.1
six==1.14.0
tensorboard==2.1.1
tensorflow-gpu==2.1.0
tensorflow-gpu-estimator==2.1.0
termcolor==1.1.0
tqdm==4.43.0
urllib3==1.25.8
Werkzeug==1.0.0
wrapt==1.12.1
yarl==1.4.2
