from sc2.ids.unit_typeid import UnitTypeId
from sc2.ids.ability_id import AbilityId
from sc2.ids.upgrade_id import UpgradeId
from sc2 import Race, Difficulty
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense
from tensorflow.keras.optimizers import Adam

# Units.
PROBE = UnitTypeId.PROBE
ZEALOT = UnitTypeId.ZEALOT
STALKER = UnitTypeId.STALKER
IMMORTAL = UnitTypeId.IMMORTAL
VOIDRAY = UnitTypeId.VOIDRAY
CARRIER = UnitTypeId.CARRIER
OBSERVER = UnitTypeId.OBSERVER

# Structures.
NEXUS = UnitTypeId.NEXUS
PYLON = UnitTypeId.PYLON
ASSIMILATOR = UnitTypeId.ASSIMILATOR
GATEWAY = UnitTypeId.GATEWAY
CYBERNETICSCORE = UnitTypeId.CYBERNETICSCORE
ROBOTICSFACILITY = UnitTypeId.ROBOTICSFACILITY
STARGATE = UnitTypeId.STARGATE
FORGE = UnitTypeId.FORGE
TWILIGHTCOUNCIL = UnitTypeId.TWILIGHTCOUNCIL
FLEETBEACON = UnitTypeId.FLEETBEACON
PHOTONCANNON = UnitTypeId.PHOTONCANNON

# Upgrades.
PROTOSSGROUNDWEAPONSLEVEL1 = UpgradeId.PROTOSSGROUNDWEAPONSLEVEL1
PROTOSSGROUNDWEAPONSLEVEL2 = UpgradeId.PROTOSSGROUNDWEAPONSLEVEL2
PROTOSSGROUNDWEAPONSLEVEL3 = UpgradeId.PROTOSSGROUNDWEAPONSLEVEL3
PROTOSSGROUNDARMORSLEVEL1 = UpgradeId.PROTOSSGROUNDARMORSLEVEL1
PROTOSSGROUNDARMORSLEVEL2 = UpgradeId.PROTOSSGROUNDARMORSLEVEL2
PROTOSSGROUNDARMORSLEVEL3 = UpgradeId.PROTOSSGROUNDARMORSLEVEL3
PROTOSSSHIELDSLEVEL1 = UpgradeId.PROTOSSSHIELDSLEVEL1
PROTOSSSHIELDSLEVEL2 = UpgradeId.PROTOSSSHIELDSLEVEL2
PROTOSSSHIELDSLEVEL3 = UpgradeId.PROTOSSSHIELDSLEVEL3
PROTOSSAIRWEAPONSLEVEL1 = UpgradeId.PROTOSSAIRWEAPONSLEVEL1
PROTOSSAIRWEAPONSLEVEL2 = UpgradeId.PROTOSSAIRWEAPONSLEVEL2
PROTOSSAIRWEAPONSLEVEL3 = UpgradeId.PROTOSSAIRWEAPONSLEVEL3
PROTOSSAIRARMORSLEVEL1 = UpgradeId.PROTOSSAIRARMORSLEVEL1
PROTOSSAIRARMORSLEVEL2 = UpgradeId.PROTOSSAIRARMORSLEVEL2
PROTOSSAIRARMORSLEVEL3 = UpgradeId.PROTOSSAIRARMORSLEVEL3
CHARGE = UpgradeId.CHARGE

# Abilities.
PRISMATICALIGNMENT = AbilityId.EFFECT_VOIDRAYPRISMATICALIGNMENT

# Bot actions constants.
ACTIONS_PER_MINUTE = 60
DECISIONS_PER_MINUTE = 6
SUPPLY_LEFT_CRITICAL = 10  # If supply left is equal or less than this, then build pylon.
ACTIONS = ['defend', 'attack']
FORGE_UPGRADES = [PROTOSSGROUNDWEAPONSLEVEL1, PROTOSSGROUNDWEAPONSLEVEL2, PROTOSSGROUNDWEAPONSLEVEL3,
                  PROTOSSGROUNDARMORSLEVEL1, PROTOSSGROUNDARMORSLEVEL2, PROTOSSGROUNDARMORSLEVEL3,
                  PROTOSSSHIELDSLEVEL1, PROTOSSSHIELDSLEVEL2, PROTOSSSHIELDSLEVEL3]
CYBERNETICSCORE_UPGRADES = [PROTOSSAIRWEAPONSLEVEL1, PROTOSSAIRWEAPONSLEVEL2, PROTOSSAIRWEAPONSLEVEL3,
                            PROTOSSAIRARMORSLEVEL1, PROTOSSAIRARMORSLEVEL2, PROTOSSAIRARMORSLEVEL3]
TWILIGHT_UPGRADES = [CHARGE]

# Building constants.
PYLON_ENEMY_BASE_OFFSET = 5  # The distance from the first nexus towards enemy base near which pylons will be built.
BASE_PYLON_DISTANCE = 15 # The max distance from the main base to potential pylon position.
MAX_WORKERS = 72  # To prevent workers to cover all supply limit.
GATEWAY_NEXUSES_RATIO = 2  # How many times gateways count exceeds nexuses count.

# Units constants.
ARMY_UNIT_TYPES = [ZEALOT, STALKER, IMMORTAL, VOIDRAY, CARRIER]  # What types of units will take part in fights.
SCOUTING_LOCATION_CHANGE_DISTANCE = 5  # The distance from the scouting location on which observer changes location.
SCOUTING_LOCATIONS_COUNT = 6
ARMY_CLUSTERING_THRESHOLD = 20
BASE_UNDER_ATTACK_DISTANCE = 20  # The distance from the base from which base is under attack.
ARMY_CENTER_DEFENDING_OFFSET = 5  # The distance from the army center to the point army units will move to attack.
ARMY_CENTER_DEFENDING_DISTANCE = 10  # The distance from the army center from which army units will attack (defending).
ARMY_CENTER_ATTACKING_OFFSET = 5  # The distance from the army center from which army units will attack (attacking).
ATTACKING_LOCATION_CHANGE_DISTANCE = 5  # The distance from the attacking location on which army changes location.

# Resources constants.
WORKERS_PER_MINERALS = 16  # How many workers can work on minerals on one base.
WORKERS_PER_VESPENE = 3  # How many workers can work on one assimilator.
WORKERS_IN_VESPENE = 1  # How many workers can be in one assimilator.
GEYSERS_PER_BASE = 2  # How many vespene geysers are on one base.
GEYSERS_BASE_MAX_DISTANCE = 10  # Max distance from the base on which the worker can build an assimilator.
TOTAL_MINERALS_PER_LOCATION = 10800
TOTAL_VESPENE_PER_LOCATION = 4500
LOCATIONS_COUNT = 16
MAX_SUPPLY = 200

# Match constants.
MAP = 'AcropolisLE'
BOT_RACE = Race.Protoss
RACES = {
    'Protoss': Race.Protoss,
    'Terran': Race.Terran,
    'Zerg': Race.Zerg
}
DIFFICULTY_LEVELS = {
    'VeryEasy': Difficulty.VeryEasy,
    'Easy': Difficulty.Easy,
    'Medium': Difficulty.Medium,
    'Hard': Difficulty.Hard,
    'Harder': Difficulty.Harder,
    'VeryHard': Difficulty.VeryHard
}

# Data constants.
REPRESENTATION_WIDTH = 60
REPRESENTATION_HEIGHT = 60
CHANNELS_COUNT = 2
COST_INTENSITY_RATIO = 0.001
RENDER_SCALE = 10
RENDER_INTENSITY = 4

# Q-learning constants.
EPISODES = 1000
EPSILON = 0.9
EPSILON_DECAY = 0.99999
DISCOUNT = 1
REPLAY_SIZE = 1_000_000
BATCH_SIZE = 128
VICTORY_REWARD = 1000
DEFEAT_REWARD = -1000

# Neural network constants.
CONVOLUTIONAL_LAYERS = [
    Conv2D(32, (5, 5), input_shape=(REPRESENTATION_WIDTH, REPRESENTATION_HEIGHT, 2), activation='relu'),
    MaxPooling2D((2, 2)),
    Conv2D(64, (5, 5), activation='relu'),
    MaxPooling2D((2, 2)),
    Conv2D(128, (5, 5), activation='relu'),
    MaxPooling2D((2, 2)),
    Flatten()
]
FEATURES_COUNT = 4  # Minerals, vespene, total supply and army supply.
DENSE_LAYERS = [
    Dense(2048 + FEATURES_COUNT, activation='relu'),
    Dense(len(ACTIONS), activation='linear')
]
OPTIMIZER = Adam
LEARNING_RATE = 0.0001
LOSS = 'mse'
METRICS = ['accuracy']
GPU_MEMORY_FRACTION = 0.9

# Loading/saving constants.
EXPERIMENT_DIRECTORY = 'experiment'
EXPERIMENT_PARAMETERS_FILENAME = 'experiment.yaml'
BOT_PARAMETERS_FILENAME = 'bot.yaml'
MODEL_DIRECTORY = 'model'
MODEL_FILENAME = 'model.h5'
OPTIMIZER_FILENAME = 'optimizer'
MEMORY_FILENAME = 'memory.npz'
LOGS_FILENAME = 'logs'
REPORT_FILENAME = 'report.txt'
